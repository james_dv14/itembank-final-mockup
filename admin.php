<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <title>DCS ItemBank | Admin</title>
  <?php include_once "head.php";?>
</head>

<body>
	
<?php $GLOBALS['role_html'] = '<a class="item" href="admin.php"><h2 class="course">Admin</h2></a>' ?>
<?php include_once "header.php";?>
<?php include_once "sidebar.php";?>
	
<section class="ui center aligned landing segment">
	<h2>Site Admin</h2>
	<ul class="boxes">
		<li><a href="courses.php"><section class="ui segment">
			<h3>Courses</h3>
			<ul>
				<li>Manage</li>
			</ul>
		</a></section></li>
		<li><a href="course_admins.php"><section class="ui segment">
			<h3>Course Admins</h3>
			<ul>
				<li>Appoint</li>
				<li>Revoke</li>
			</ul>
		</a></section></li>
		<li><a href="users.php"><section class="ui segment">
			<h3>Users</h3>
			<ul>
				<li>Manage</li>
			</ul>
		</a></section></li>
		<li><a href="landing.php"><section class="ui segment">
			<h3>Back</h3>
		</a></section></li>
	</ul>
</section>

<?php include_once "footer.php";?>
<?php include_once "foot.php";?>

</body>
</html>
