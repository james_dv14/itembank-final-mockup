<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <title>DCS ItemBank | Course Roles</title>
  <?php include_once "head.php";?>
</head>

<body>

<?php $GLOBALS['role_html'] = '<a class="item" href="admin.php"><h2 class="course">Admin</h2></a>' ?>
<?php include_once "header.php";?>
<?php include_once "sidebar.php";?>
	
<section class="ui center aligned landing segment">
	<h2>Course Admins</h2>
	<ul>
		<li><section>
			<h3>CS 145</h3>
			<ul>
				<li>mtcarreon (<a href="#">revoke</a>)</li>
			</ul><br>
			<a href="#">Appoint admin</a>
		</section></li>
		<li><section>
			<h3>CS 153</h3>
			<ul>
				<li>pczuniga (<a href="#">revoke</a>)</li>
				<li>mtcarreon (<a href="#">revoke</a>)</li>
			</ul><br>
			<a href="#">Appoint admin</a>
		</section></li>
		<li><section>
			<h3>CS 192</h3>
			<ul>
				<li>epfelizmenio (<a href="#">revoke</a>)</li>
			</ul><br>
			<a href="#">Appoint admin</a>
		</section></li>
	</ul>
	<a href="admin.php">Back</a>
</section>

<?php include_once "footer.php";?>
<?php include_once "foot.php";?>

</body>
</html>
