<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <title>DCS ItemBank | Course Roles</title>
  <?php include_once "head.php";?>
</head>

<body>

<?php $GLOBALS['role_html'] = '<a class="item" href="cs192.php"><h2 class="course">CS 192</h2></a>' ?>
<?php include_once "header.php";?>
<?php include_once "sidebar.php";?>
	
<section class="ui center aligned landing segment">
	<h2>Course Roles</h2>
	<form method="post" action="landing.php">
		<label for="user">User</label><br>
		<select multiple name="user">
		  <option value="-">-</option>
		  <option value="edgar">epfelizmenio</option>
		  <option value="mario">mtcarreon</option>
		  <option value="philip">pczuniga</option>
		</select>
		<br><br>
		<label for="role">Role</label><br>
		<select name="role_set">
		  <option value="instructor">Instructor</option>
		  <option value="editor">Editor</option>
		</select>
		<br>
		<input type="radio" name="role" value="Grant">Grant<br>
		<input type="radio" name="role" value="Set">Set<br>
		<input type="radio" name="role" value="Revoke">Revoke<br>
		<br>
		<input type="submit" value="Commit">
	</form>
	<br>
	<a href="cs192.php">Back</a>
</section>

<?php include_once "footer.php";?>
<?php include_once "foot.php";?>

</body>
</html>