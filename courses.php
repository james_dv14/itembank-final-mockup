<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <title>DCS ItemBank | Courses</title>
  <?php include_once "head.php";?>
</head>

<body>

<?php $GLOBALS['role_html'] = '<a class="item" href="admin.php"><h2 class="course">Admin</h2></a>' ?>
<?php include_once "header.php";?>
<?php include_once "sidebar.php";?>
	
<section class="ui center aligned landing segment">
	<h2>Courses</h2>
	
	<ul class="boxes">
		<li><section class="ui center aligned compact segment">
			<h3>CS 145</h3>
			<p class="description">Computer Networks</p>
			<div class="ui primary button"><a href="#">Edit</a></div>
			<div class="ui primary button"><a href="#">Delete</a></div>
		</section></li>
		<li><section class="ui center aligned compact segment">
			<h3>CS 153</h3>
			<p class="description">Intro to Computer Security</p>
			<div class="ui primary button"><a href="#">Edit</a></div>
			<div class="ui primary button"><a href="#">Delete</a></div>
		</section></li>
		<li><a href="#"><section class="ui segment">
			<h3>Add Course</h3>
		</a></section></li>
		<li><a href="admin.php"><section class="ui segment">
			<h3>Back</h3>
		</a></section></li>
	</ul>
	
	<table>
		<thead>
			<th>Course</th>
			<th>Name</th>
			<th>Edit</th>
			<th>Delete</th>
		</thead>
		<tbody>
			<tr>
			<td><a href="#">CS 145</a></td>
			<td>Computer Networks</td>
			<td><a href="#">Edit</a></td>
			<td><a href="#">Delete</a></td>
			</tr>
			<tr>
			<td><a href="#">CS 153</a></td>
			<td>Intro to Computer Security</td>
			<td><a href="#">Edit</a></td>
			<td><a href="#">Delete</a></td>
			</tr>
			<tr>
			<td><a href="#">CS 192</a></td>
			<td>Software Engineering II</td>
			<td><a href="#">Edit</a></td>
			<td><a href="#">Delete</a></td>
			</tr>
		</tbody>
	</table>
	<a href="#">Add course</a><br>
	<a href="admin.php">Back</a>
</section>

<?php include_once "footer.php";?>
<?php include_once "foot.php";?>

</body>
</html>
