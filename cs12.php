<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <title>DCS ItemBank | CS 12</title>
  <?php include_once "head.php";?>
</head>

<body>
	
<?php $GLOBALS['role_html'] = '<a class="item" href="cs12.php"><h2 class="course">CS 12</h2></a>' ?>
<?php include_once "header.php";?>
<?php include_once "sidebar.php";?>
	
<section class="ui center aligned landing segment">
	<h2>CS 12 (Course Instructor)</h2>
	<ul class="boxes">
		<li><section class="ui segment">
			<h3>Items</h3>
			<ul>
				<li>Manage Own</li>
				<li>View Others'</li>
			</ul>
		</section></li>
		<li><section class="ui segment">
			<h3>Probsets</h3>
			<ul>
				<li>Manage Own</li>
				<li>View Others'</li>
			</ul>
		</section></li>
		<li><section class="ui segment">
			<h3>Course</h3>
			<ul>
				<li>View Info</li>
				<li>View Topics</li>
			</ul>
		</section></li>
		<li><a href="landing.php"><section class="ui segment">
			<h3>Back</h3>
		</a></section></li>
	</ul>
</section>

<?php include_once "footer.php";?>
<?php include_once "foot.php";?>

</body>
</html>
