<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <title>DCS ItemBank | CS 192</title>
  <?php include_once "head.php";?>
</head>

<body>

<?php $GLOBALS['role_html'] = '<a class="item" href="cs192.php"><h2 class="course">CS 192</h2></a>' ?>
<?php include_once "header.php";?>
<?php include_once "sidebar.php";?>
	
<section class="ui center aligned landing segment">
	<h2>CS 192 (Course Admin)</h2>
	<ul class="boxes">
		<li><section class="ui segment">
			<h3>Items</h3>
			<ul>
				<li>Manage Own</li>
				<li>Manage Others'</li>
			</ul>
		</section></li>
		<li><section class="ui segment">
			<h3>Probsets</h3>
			<ul>
				<li>Manage Own</li>
				<li>Manage Others'</li>
			</ul>
		</section></li>
		<li><section class="ui segment">
			<h3>Course</h3>
			<ul>
				<li>Manage Info</li>
				<li>Manage Topics</li>
			</ul>
		</section></li>
		<li><a href="course_roles.php"><section class="ui segment">
			<h3>Course Roles</h3>
			<ul>
				<li>Grant</li>
				<li>Set</li>
				<li>Revoke</li>
			</ul>
		</section></a></li>
		<li><a href="landing.php"><section class="ui segment">
			<h3>Back</h3>
		</a></section></li>
	</ul>
</section>

<?php include_once "footer.php";?>
<?php include_once "foot.php";?>

</body>
</html>
