<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <title>DCS ItemBank</title>
  <?php include_once "head.php";?>
</head>

<body>
	
<?php $GLOBALS['role_html'] = "" ?>
<?php include_once "header.php";?>
<?php include_once "sidebar.php";?>

<section class="ui center aligned landing segment">
	<h3>Welcome, user. Please select a session role.</h3>
	<ul class="boxes">
		<li><a href="admin.php"><section class="ui segment">
			<h3>Site Admin</h3>
		</a></section></li>
		<li><a href="cs12.php"><section class="ui segment">
			<h3>CS 12</h3>
			<p>Instructor</p>
		</a></section></li>
		<li><a href="cs191.php"><section class="ui segment">
			<h3>CS 191</h3>
			<p>Editor</p>
		</a></section></li>
		<li><a href="cs192.php"><section class="ui segment">
			<h3>CS 192</h3>
			<p>Course Admin</p>
		</a></section></li>
		<li><a href="index.php"><section class="ui segment">
			<h3>Logout</h3>
		</a></section></li>
	</ul>
</section>

<?php include_once "footer.php";?>
<?php include_once "foot.php";?>

</body>
</html>
