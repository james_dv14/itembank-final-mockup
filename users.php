<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
  <title>DCS ItemBank | Users</title>
  <?php include_once "head.php";?>
</head>

<body>

<?php $GLOBALS['role_html'] = '<a class="item" href="admin.php"><h2 class="course">Admin</h2></a>' ?>
<?php include_once "header.php";?>
<?php include_once "sidebar.php";?>
	
<section class="ui center aligned landing segment">
	<h2>Users</h2>
	<table>
		<thead>
			<th>Name</th>
			<th>Username</th>
			<th>Edit</th>
			<th>Delete</th>
		</thead>
		<tbody>
			<tr>
			<td>CARREON, Mario T.</td>
			<td><a href="#">mtcarreon</a></td>
			<td><a href="#">Edit</a></td>
			<td><a href="#">Delete</a></td>
			</tr>
			<tr>
			<td>FELIZMENIO, Edgar P.</td>
			<td><a href="#">epfelizmenio</a></td>
			<td><a href="#">Edit</a></td>
			<td><a href="#">Delete</a></td>
			</tr>
			<tr>
			<td>ZUNIGA, Philip C.</td>
			<td><a href="#">pczuniga</a></td>
			<td><a href="#">Edit</a></td>
			<td><a href="#">Delete</a></td>
			</tr>
		</tbody>
	</table>
	<a href="#">Add user</a><br>
	<a href="admin.php">Back</a>
</section>

<?php include_once "footer.php";?>
<?php include_once "foot.php";?>

</body>
</html>
